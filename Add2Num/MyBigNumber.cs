﻿namespace Add2Num;
using System.Text;

    public class MyBigNumber
    {
    /*
       public  string Sum(string stn1, string stn2)
    {
        StringBuilder sb = new StringBuilder();
        int i = stn1.Length - 1;
        int j = stn2.Length - 1;
        char[] num1Chars = stn1.ToCharArray();
        char[] num2Chars = stn2.ToCharArray();
        int carry = 0;
        while (i >= 0 || j >= 0)
        {
            int sum = carry;
            if (i >= 0)
            {
                sum += int.Parse(num1Chars[i].ToString());
                i--;
            }
            if (j >= 0)
            {
                sum += int.Parse(num2Chars[j].ToString());

                j--;
            }
            sb.Insert(0, sum % 10); // to get reminder
            carry = sum / 10;
        }
        if (carry > 0) sb.Insert(0, 1);
        return sb.ToString();
    //    Console.WriteLine("KQ: " + sb.ToString());
    }

   */

     public string Sum (string nb1, string nb2)
    {
        int nb1Length = nb1.Length;
        int nb2Length = nb2.Length;
        int lenMax= nb1Length > nb2Length ? nb1Length : nb2Length;
        string result = String.Empty;
        int temp;
        int idx1, idx2;
        char c1, c2;
        int d1, d2;
        int mem = 0;
        for (int i=0 ; i < lenMax; i++)
        {
            idx1 = nb1Length - i - 1;
            idx2 = nb2Length - i - 1;
            c1 = (idx1 >= 0) ? nb1[idx1] : '0';
            c2 = (idx2 >= 0) ? nb2[idx2] : '0';
            d1 = c1 - '0';
            d2 = c2 - '0';
            temp = d1 + d2 + mem;
            mem = temp / 10;
            temp = temp % 10;
            result = temp + result;
        }
        if (mem >0)
        {
            result = mem + result;
        }
        return result;
    }

    static void Main()
        {
            Console.Write("Enter a number 1: ");
            string num1 = Convert.ToString(Console.ReadLine());
            Console.Write("Enter a number 2: ");
            string num2 = Convert.ToString(Console.ReadLine());
     
          
            
        }

   
}

