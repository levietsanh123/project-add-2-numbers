using Add2Num;

namespace TestAdd2Num
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            string sum = myBigNumber.Sum("123456789","987654321");
            //    Assert.Pass("4", sum);
            Assert.AreEqual("1111111110", sum);
            
        }
        [Test]
        public void Test2()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            string sum = myBigNumber.Sum("111", "222");
         //   Assert.Pass("123", sum);    
            Assert.AreEqual("333", sum);  
        }
    }
}